/**
 * @file
 *
 * @date Jul 18, 2022
 * @author Anton Bondarev
 */

#ifndef EXT_PROJECT_ERLANG_INCLUDE_EMBOX_ERLANG_COMPAT_H_
#define EXT_PROJECT_ERLANG_INCLUDE_EMBOX_ERLANG_COMPAT_H_

#define THREAD_SCOPE_PROCESS  1
#define THREAD_SCOPE_SYSTEM   2

#include <pthread.h>

extern
int pthread_attr_setstacksize(pthread_attr_t *attr, size_t stacksize);

extern
int pthread_attr_setscope(pthread_attr_t *, int);



#endif /* EXT_PROJECT_ERLANG_INCLUDE_EMBOX_ERLANG_COMPAT_H_ */
